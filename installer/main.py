import os
import subprocess

CONFIG = {
    "isla": {
        "install_device": "/dev/sda",
        "label": "msdos",
        "kernel": "sys-kernel/gentoo-sources",
        "firmware": False,
        "boot": {
            "index": 1,
            "fs": "btrfs",
            # Size in MB
            "size": 1024
        },
        "root": {
            "index": 2,
            "fs": "btrfs",
            # Size in MB
            "size": -1
        },
        "ansible_vars": "/",
        "kernel_config": "src/kernel/kernel.vm",
        "profile": "default/linux/amd64/17.0"
    }
}
