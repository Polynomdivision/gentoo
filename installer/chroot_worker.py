import socket
import json
import subprocess

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.connect("/tmp/chroot_worker")

while 1:
    data = sock.recv(1024)
    if not data:
        break

    data = data.decode("UTF8")
    obj = json.loads(data)

    if obj["op"] == "exit":
        break
    elif obj["op"] == "cmd":
        # Did we get environment variables?
        env = obj["env"] if "env" in obj.keys() else {}
        # Did we get a cwd?
        cwd = obj["cwd"] if "cwd" in obj.keys() else "/"
        proc = subprocess.run(obj["cmd"],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE,
                              cwd=cwd,
                              env=env)

        sock.send(json.dumps({
            "exit": proc.returncode(),
            # "stdout": proc.stdout(),
            "stderr": proc.stderr()
        }).encode())

sock.close()
