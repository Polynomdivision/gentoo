#!/usr/bin/env python3
import os
import subprocess
import socket
import sys
import json

CONFIG = {
    "isla": {
        "install_device": "/dev/sda",
        "label": "msdos",
        "kernel": "sys-kernel/gentoo-sources",
        "firmware": False,
        "boot": {
            "index": 1,
            "fs": "btrfs",
            # Size in MB
            "size": 1024
        },
        "root": {
            "index": 2,
            "fs": "btrfs",
            # Size in MB
            "size": -1
        },
        "kernel_config": "kernel.vm",
        "profile": "default/linux/amd64/17.0",
        "inventory": "vm",
    }
}

STAGE3_BASE = "http://distfiles.gentoo.org/releases/amd64/autobuilds/20190210T214503Z/{}"
STAGE3_NAME = "stage3-amd64-20190210T214503Z.tar.xz"
STAGE3_URL = STAGE3_BASE.format(STAGE3_NAME)
STAGE3_DIGEST = STAGE3_BASE.format(STAGE3_NAME + ".DIGESTS")
STAGE3_DIGEST_SIG = STAGE3_BASE.format(STAGE3_NAME + ".DIGESTS.asc")
REPO_BASE = "https://gitlab.com/Polynomdivision/gentoo/raw/master"
DEBUG = "GENTOO_DEBUG" in os.environ.keys()

CHROOT_SOCK = None
CHROOT_PROC = None

def has_efi(cfg):
    return "efi" in cfg.keys()

def has_swap(cfg):
    return "swap" in cfg.keys()

def repo_file_link(f):
    return "{}/{}".format(REPO_BASE, f)

def execute_command(cmd):
    """
    Executes a command using the shell and returns the stdout
    output.
    """
    if not DEBUG:
        proc = subprocess.run(cmd, stdout=subprocess.PIPE)
        return proc.returncode()

    print("Debug: " + str(cmd))
    return 0

def execute_chroot(cmd, cwd=""):
    """
    Executes inside a chroot
    """
    if not DEBUG:
        # Check if we have the socket established
        if not CHROOT_SOCK:
            print("Error: No socket connection")
            return ""

        CHROOT_SOCK.send(json.dumps({
            "op": "cmd",
            "cwd": cwd,
            "cmd": cmd
        }).encode())

        # Wait for the result
        res = CHROOT_SOCK.recv(1024).decode("UTF8")
        return json.loads(res)

    print("Debug({}): (chroot) {}".format(cwd, str(cmd)))
    return ""

def partition():
    def mb(num):
        return str(num) + "MB"

    # Get the correct config
    dev = CONFIG[HOSTNAME]

    boot = dev["boot"]
    root = dev["root"]

    start = 1
    part = 1
    parted_cmd = ["parted", "--script", dev["install_device"], "\\",
                  "mklabel", dev["label"], "\\"] #,
    mkfs_cmd = []

    # Do we have an EFI partition?
    # TODO
    if "efi" in dev.keys():
        parted_cmd += ["mkpart", "primary", "vfat", mb(start), mb(start + 1024), "\\"]
        start += 1024

        mkfs_cmd += [["mkfs.vfat", "-F32", dev["install_device"] + str(part)]]
        CONFIG[HOSTNAME]["efi"]["index"] = part
        part += 1

    # Boot
    parted_cmd += ["mkpart", "primary", boot["fs"], mb(start), mb(start + 2048), "\\"]
    start += 2048
    CONFIG[HOSTNAME]["boot"]["index"] = part
    mkfs_cmd += [["mkfs." + boot["fs"], dev["install_device"] + str(part)]]
    part += 1


    # Do we have swap?
    if "swap" in dev.items():
        size = dev["swap"]["size"]
        parted_cmd += ["mkpart", "primary", "linux-swap", mb(start), mb(start + size), "\\"]
        start += size
        CONFIG[HOSTNAME]["swap"]["index"] = part
        mkfs_cmd += [["mkswap", dev["install_device"] + str(part)],
                     ["swapon", dev["install_device"] + str(part)]]
        part += 1


    # Root
    parted_cmd += ["mkpart", "primary", root["fs"], mb(start), "-1"]
    CONFIG[HOSTNAME]["root"]["index"] = part
    mkfs_cmd += [["mkfs." + root["fs"], dev["install_device"] + str(part)]]
    part += 1

    # Execute
    execute_command(parted_cmd)

    # Make filesystems
    for mkfs in mkfs_cmd:
        execute_command(mkfs)


def prepare_mounts():
    dev = CONFIG[HOSTNAME]

    # First root
    execute_command(["mount", dev["install_device"] + str(dev["root"]["index"]),
                     "/mnt/gentoo"])

    # Then boot
    # Make sure it exists
    execute_command(["mkdir", "-p", "/mnt/gentoo/boot"])
    execute_command(["mount", dev["install_device"] + str(dev["boot"]["index"]),
                     "/mnt/gentoo/boot"])

    # EFI?
    # if has_efi(dev):
    #     execute_command(["mount", dev["install_device"] + str(dev["efi"]["index"])])

def get_device_uuid(device_path):
    out = execute_command(["blkid", "--output", "value", device_path]).decode("UTF8")
    return out.split("\n")[0]


def prepare_chroot_socket():
    # Create the socket
    if not DEBUG:
        global CHROOT_SOCK
        CHROOT_SOCK = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        CHROOT_SOCK.bind("/tmp/installer.sock")
    else:
        print("Creating unix socket for command sending")

    # Symlink into the chroot
    execute_command(["ln",
                     "-sf",
                     "/tmp/installer.sock",
                     "/mnt/gentoo/tmp/installer.sock"])
    execute_command(["ln",
                     "-sf",
                     "{}/chroot_worker.py".format(os.path.realpath(__file__)),
                     "/mnt/gentoo/tmp/chroot_worker.py"])

    # Start the command listener
    if not DEBUG:
        global CHROOT_PROC
        CHROOT_PROC = subprocess.Popen(["chroot",
                                        "/mnt/gentoo",
                                        "/bin/sh",
                                        "-c",
                                        "python",
                                        "/tmp/chroot_worker.py"])
    else:
        print("Starting command listener in the chroot")

def prepare_installation():
    dev = CONFIG[HOSTNAME]

    # Time sync
    execute_command(["ntpd", "-q", "-g"])

    # Download the stage3 tarball
    # TODO: Check signature
    execute_command(["wget", "-O", "/mnt/gentoo/stage3.tar.xz", STAGE3_URL])
    execute_command(["wget", "-O", "/mnt/gentoo/stage3.tar.xz.dig", STAGE3_DIGEST])
    execute_command(["wget", "-O", "/mnt/gentoo/stage3.tar.xz.dig.asc", STAGE3_DIGEST_SIG])

    # Import GPG keys
    execute_command(["gpg", "--keyserver", "hkps.pool.sks-keyservers.net",
                     "--recv-key", "0xBB572E0E2D182910"])
    ret = execute_command(["gpg", "--verify", "/mnt/gentoo/stage3.tar.xz.dig.asc"])
    if ret != 0:
        print("Verification of digests file failed!")
        sys.exit(1)

    ret = execute_command(["sha512sum", "-c", "/mnt/gentoo/stage3.tar.xz.dig.asc"])
    if ret != 0:
        print("Verification of stage3 failed!")
        sys.exit(1)

    # Unpack the stage3 archive
    execute_command(["tar", "-xpf", "-C", "/mnt/gentoo", "/mnt/gentoo/stage3.tar.xz"])

    # Install make.conf
    execute_command(["mkdir", "-p", "/mnt/gentoo/etc/portage"])
    execute_command(["cp",
                     "{}/../src/portage/make.conf",
                     "/mnt/gentoo/etc/portage/make.conf"])

    # Prepare repos
    execute_command(["mkdir", "-p", "/mnt/gentoo/etc/portage/repos.conf"])
    execute_command(["cp",
                     "/mnt/gentoo/etc/portage/config/repos.conf",
                     "/mnt/gentoo/etc/portage/repos.conf/gentoo.conf"])

    # Copy over DNS info
    execute_command(["cp",
                     "--dereference",
                     "/etc/resolv.conf",
                     "/mnt/gentoo/etc/"])

    # Bind mount host filesystems
    execute_command(["mount", "--types", "proc", "/proc", "/mnt/gentoo/proc"])
    execute_command(["mount", "--rbind", "/sys", "/mnt/gentoo/sys"])
    execute_command(["mount", "--rbind", "/dev", "/mnt/gentoo/dev"])
    execute_command(["mount", "--make-rslave", "/mnt/gentoo/sys"])
    execute_command(["mount", "--make-rslave", "/mnt/gentoo/dev"])

    prepare_chroot_socket()

    # Select the profile
    profiles = execute_chroot(["eselect", "profile", "list"]).split("\n")
    profile_line = ""
    for line in profiles:
        if dev["profile"] in line:
            profile_line = line

    # TODO: CHECK
    profile_num = profile_line.lstrip().split(" ")[0].strip("[] ")
    execute_chroot(["eselect", "profile", "set", profile_num])

    # Update @world
    execute_chroot(["emerge",
                    "--ask",
                    "--verbose",
                    "--update",
                    "--deep",
                    "--newuse",
                    "@world"])

    # Configure timezone data
    if not DEBUG:
        open("/etc/timezone", "w").write("Europe/Berlin")
    execute_chroot(["emerge", "--config", "sys-libs/timezone-data"])

    if not DEBUG:
        open("/etc/locale.gen", "w").write("""de_DE.UTF-8 UTF-8
en_GB.UTF-8 UTF-8
ja_JP.UTF-8 UTF-8""")
    else:
        print("Writing locale data")
    execute_chroot(["locale-gen"])

    # Set the locale
    if not DEBUG:
        open("/etc/env.d/02locale").write("""LANG=\"en_GB.UTF-8\"
LC_COLLATE=\"C\"""")
    else:
        print("Writing locale settings")

def make_kernel():
    dev = CONFIG[HOSTNAME]

    # Install the kernel
    execute_chroot(["emerge", "--ask", dev["kernel"]])
    execute_command(["cp",
                     "{}/../src/kernel/{}".format(os.path.realpath(__file__),
                                                  dev["kernel_config"]),
                     "/mnt/gentoo/usr/src/linux/.config"])

    for cmd in (["make", "-j6"], ["make", "modules_install"], ["make", "install"]):
        # subprocess.run(cmd, cwd="/usr/src/linux")
        execute_chroot(cmd, cwd="/usr/src/linux")

#HOSTNAME = execute_command(["hostname"]).strip("\r\n ")
HOSTNAME = "isla"

partition()
prepare_mounts()
prepare_installation()
make_kernel()

# Install ansible and execute our playbook
execute_chroot(["emerge", "--ask", "app-admin/ansible"])
execute_command(["ln",
                 "-sfT",
                 "{}/../".format(os.path.realpath(__file__)),
                 "/mnt/gentoo/tmp/gentoo"])
inventory = CONFIG[HOSTNAME]["inventory"]
execute_chroot(["ansible-playbook",
                "-i", "/tmp/gentoo/ansible/inventories/{}".format(inventory),
                "/tmp/gentoo/ansible/main.yml"])

# Shutdown the worker
if not DEBUG:
    CHROOT_SOCK.send(b"{\"op\": \"exit\"}")
else:
    print("Shutting down chroot worker")
